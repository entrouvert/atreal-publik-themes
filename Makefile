VERSION=`git describe | sed 's/^v//; s/-/./g'`
NAME="atreal-publik-themes"

prefix = /usr

all: themes.json css

themes.json: $(wildcard static/*/config.json)
	echo "[]" > themes.json

%.css: export LC_ALL=C.UTF-8
.SECONDEXPANSION:
%.css: %.scss $$(wildcard $$(@D)/*.scss)
	sassc $< $@

publik-base-theme/static/includes/_data_uris.scss: $(wildcard publik-base-theme/static/includes/img/*)
	cd publik-base-theme; python3 make_data_uris.py static/includes/

css: publik-base-theme/static/includes/_data_uris.scss \
     publik-base-theme/static/includes/_publik.scss    \
     static/concarneau/style.css   \
     static/coprec/style.css   \
     static/avray/style.css    \
     static/malakoff/style.css
	rm -rf static/*/.sass-cache/

clean:
	rm -f themes.json
	rm -f static/*/style.css
	rm -rf sdist
	rm -f static/*/_data_uris.scss
	rm -rf publik-base-theme/static/includes/_data_uris.scss
	rm -f publik-base-theme/static/*/style.css

DIST_FILES = \
	README \
	Makefile \
	publik-base-theme \
	static templates

dist: clean
	-mkdir sdist
	rm -rf sdist/$(NAME)-$(VERSION)
	mkdir -p sdist/$(NAME)-$(VERSION)
	for i in $(DIST_FILES); do \
		cp -R "$$i" sdist/$(NAME)-$(VERSION); \
	done

install:
	mkdir -p $(DESTDIR)$(prefix)/share/publik/themes/atreal
	cp -r static templates themes.json $(DESTDIR)$(prefix)/share/publik/themes/atreal
	rm $(DESTDIR)$(prefix)/share/publik/themes/atreal/static/*/config.json

dist-bzip2: dist
	-mkdir sdist
	cd sdist && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 $(NAME)-$(VERSION)

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))
